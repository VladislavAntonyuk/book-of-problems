<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" />
<script src="https://code.jquery.com/jquery-1.12.4.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js" type="text/javascript"></script>
<p>
<button onclick="$('.newTask').toggle(400);">Добавить новую задачу</button>

<form class="newTask" style="display:none" method="post">
	<input type="text" required name="username" placeholder="Имя пользователя"/><br>
	<input type="email" required name="email" placeholder="Email"/><br>
	<textarea name="task" required placeholder="Текст задачи"></textarea><br>
	<input type="file" name="userImage" id="userImage" class="user-image" required /><br>
    <button type="button" onclick="readURL(document.getElementsByName('userImage')[0]); return false;">Предварительный просмотр</button><br>
	<img id="blah" style="display:none;" src="#" alt="your image" /><br>
	<input type="submit" class="saveBtn" value="Сохранить"/>
</form>

<table id="example" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Статус выполнения</th>
                <th>Имя пользователя</th>
                <th>Email</th>
                <th>Текст</th>
                <th>Изображение</th>
            </tr>
        </thead>
<?php

	foreach($data as $row)
	{
		echo '<tr><td><span style="display:none">'.$row['isDone'].'</span><input class="status" id='.$row["id"].' type="checkbox" '; 
		
		if ($row['isDone']==1) echo 'checked';
		
		if ( $_SESSION['admin'] != "123" )
		{
			echo " disabled";
		}
		
		echo '/></td><td>'.$row['username'].'</td><td>'.$row["email"].'</td><td>';
                if ( $_SESSION['admin'] != "123" )
		{
			echo $row["text"];
		}
		else
		{
		        echo "<textarea class='editTask' id=".$row["id"].">". $row["text"] ."</textarea><button class='saveBtn'>Сохранить</button>";
		}
                echo '</td><td><img src="images/'.$row["img"].'" style="max-width:320px;max-height:240px"/></td></tr>';
	}
?>
</table>
</p>
<script>
$(document).ready(function() {
    $('#example').DataTable( {
        "order": [[ 0, "desc" ]]
    });
});

$(".status").change(function () {
	var checkId=$(this).attr("id");
	var stat=this.checked;
	$.post("main/update", { id: checkId, uData: stat  });
});

$(".saveBtn").click(function () {
	var taskId=$(this).prev().attr("id");
	var task=$(this).prev().val();
	$.post("main/update", { id: taskId, uData: task  });
});
 
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result)
                .css({
						"max-width": "320px", 
						"max-height": "240px"
				})        
		};

        reader.readAsDataURL(input.files[0]);
		$("#blah").show();	
    }
}

$(".newTask").on('submit',(function(e) {
		e.preventDefault();	
		$.ajax({
			url: "main/insert",
			type: "POST",
			data: new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data)
			{
				location.reload();
			}
		});
	}
));
</script>