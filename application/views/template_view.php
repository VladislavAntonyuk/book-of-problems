<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<title>Задачник</title>
		<link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css" />
		<link href="http://fonts.googleapis.com/css?family=Kreon" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="/css/style.css" />
	</head>
	<body>
		<div id="wrapper">
			<div id="header">
				<div id="logo">
					<a href="/"><h1>Задачник</h1></a>
				</div>
				<div id="menu">
					<ul>
						<li class="last">
						<?php
						    if ($_SESSION['admin'] != "123" )
							{
								echo "<a href='/login'>Вход</a>";
							}
							else
							{
							    echo "<a href='/admin/logout'>Выход</a>";
							}
						?>
                                                </li>
					</ul>
					<br class="clearfix" />
				</div>
			</div>
			<div id="page">
				<?php include 'application/views/'.$content_view; ?>
				<br class="clearfix" />
			</div>
		</div>
		<div id="footer">
			<a href="/">Vladislav Antonyuk</a> &copy; <?php echo date("Y");?>
		</div>
	</body>
</html>	