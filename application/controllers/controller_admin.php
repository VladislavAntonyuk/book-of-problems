<?php

class Controller_Admin extends Controller
{
	
	function action_index()
	{
		session_start();
		
		if ( $_SESSION['admin'] == "123" )
		{
			//$this->view->generate('admin_view.php', 'template_view.php');
			header('Location:/');
		}
		else
		{
			session_destroy();
			Route::ErrorPage404();
		}

	}
	
	function action_logout()
	{
		session_start();
		session_destroy();
		header('Location:/');
	}

}
		