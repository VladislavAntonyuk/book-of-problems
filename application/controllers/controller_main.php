<?php

class Controller_Main extends Controller
{

	function __construct()
	{
		$this->model = new Model_Main();
		$this->view = new View();
	}
	
	function action_index()
	{
		session_start();
		$data = $this->model->get_data();		
		$this->view->generate('main_view.php', 'template_view.php', $data);
	}
	
	function action_insert()
	{
		$this->model->insert(
			$_REQUEST["username"],
			$_REQUEST["email"], 
			$_REQUEST["task"],
			$_FILES["userImage"]["name"]);
		$this->uploadImage();
	}
	
	function action_update()
	{
		$this->model->update($_REQUEST["id"],$_REQUEST["uData"]);
	}
	
	function uploadImage()
	{
	    $extensions = array("jpeg", "jpg", "png");
	    $fileTypes = array("image/png","image/jpg","image/jpeg");
	    $file = $_FILES["userImage"];
	    $file_extension = strtolower(end(explode(".", $file["name"])));
	    if (in_array($file["type"],$fileTypes) 
			&& in_array($file_extension, $extensions))
        {
	       $sourcePath = $file['tmp_name'];
	       $targetPath = 'images/'.$file['name'];
            $this->resize($sourcePath, $targetPath, 320, 240, $file_extension);
	    }
    }


	function resize($target, $newcopy, $w, $h, $ext) {
		list($w_orig, $h_orig) = getimagesize($target);
		$scale_ratio = $w_orig / $h_orig;
		if (($w / $h) > $scale_ratio) 
		{
			$w = $h * $scale_ratio;
		}
		else
		{
			$h = $w / $scale_ratio;
		}
		
		$img = "";
		if ($ext == "gif")
		{ 
			$img = imagecreatefromgif($target);
		} 
		else if($ext =="png"){ 
			$img = imagecreatefrompng($target);
		} 
		else { 
			$img = imagecreatefromjpeg($target);
		}
		
		$tci = imagecreatetruecolor($w, $h);
		imagecopyresampled($tci, $img, 0, 0, 0, 0, $w, $h, $w_orig, $h_orig);
		imagejpeg($tci, $newcopy, 80);
	}
}
